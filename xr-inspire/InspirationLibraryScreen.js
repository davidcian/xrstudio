import React, { Component } from 'react';
import { View, Text, FlatList } from 'react-native';
import { Card, Button } from 'react-native-elements';

const PROJECT_DATA = [
  {
    id: 1,
    title: 'Project 1'
  },
  {
    id: 2,
    title: 'Project 2'
  }
]

function ProjectCard({ title, imageSrc, selectProject }) {
  return (
    <Card title={title} image={imageSrc} containerStyle={{ margin: 10 }} imageStyle={{ width: 100, height: 100 }}>
      <Button title="Try me!" onPress={selectProject} />
    </Card>
  )
}

export class InspirationLibraryScreen extends Component {
  render() {
    return (
      <View>
        <FlatList
          data={PROJECT_DATA}
          renderItem={({item}) => <ProjectCard title={item.title} imageSrc={item.src} selectProject={() => this.props.navigation.push('ProjectOverview')} />}
          keyExtractor={item => item.id}
          numColumns={3}
        />
      </View>
    )
  }
}