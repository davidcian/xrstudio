import React, { Component } from 'react';
import { View, Text, FlatList, Button } from 'react-native';
import { Card } from 'react-native-elements';

function InspirationArtifactCard({ title, imageSrc }) {
  return (
    <Card title={title} image={imageSrc} containerStyle={{ margin: 10 }} imageStyle={{ width: 100, height: 100 }}>

    </Card>
  )
}

export class ProjectOverviewScreen extends Component {
  state = {
    inspirationData: [
      {
        id: 1,
        title: 'Project 1',
        src: require('./assets/icon.png')
      },
      {
        id: 2,
        title: 'Project 2',
        src: require('./assets/icon.png')
      }
    ]
  }

  startPhotoInspiration = () => {
    this.props.navigation.navigate('Camera')
  }

  render() {
    return (
      <View>
        <FlatList
          data={this.state.inspirationData}
          renderItem={({item}) => <InspirationArtifactCard title={item.title} imageSrc={item.src} />}
          keyExtractor={item => item.id}
          numColumns={3}
        />
        <Button title="Add evidence" onPress={this.startPhotoInspiration} />
      </View>
    )
  }
}