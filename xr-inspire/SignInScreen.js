import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Button, Input } from 'react-native-elements';

export class SignInScreen extends Component {
  render() {
    return (
      <View>
        <Input placeholder="E-mail" />
        <Input placeholder="Password" />
        <Button title="Sign In" onPress={() => this.props.navigation.navigate('SignedIn')} />
      </View>
    )
  }
}