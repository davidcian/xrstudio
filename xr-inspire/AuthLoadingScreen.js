import React, { Component } from 'react';
import { View, Text } from 'react-native';

export class AuthLoadingScreen extends Component {
  componentDidMount() {
    this._bootstrapAsync()
  }

  _bootstrapAsync = async () => {
    this.props.navigation.navigate('SignedOut')
  }

  render() {
    return (
      <View>
        <Text>Loading auth...</Text>
      </View>
    )
  }
}