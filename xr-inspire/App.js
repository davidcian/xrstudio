import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import { AuthLoadingScreen } from './AuthLoadingScreen';
import { SignInScreen } from './SignInScreen';
import { SignUpScreen } from './SignUpScreen';
import { InspirationLibraryScreen } from './InspirationLibraryScreen';
import { ProfileScreen } from './ProfileScreen';
import { ProjectOverviewScreen } from './ProjectOverviewScreen';
import { CameraScreen } from './CameraScreen';

export default function App() {
  return (
    <AppContainer />
  );
}

const SignedOutNavigator = createStackNavigator(
  { 
    SignIn: SignInScreen, 
    SignUp: SignUpScreen 
  },
  {
    initialRouteName: 'SignIn'
  }
)

const InspirationNavigator = createStackNavigator(
  {
    InspirationLibrary: InspirationLibraryScreen,
    ProjectOverview: ProjectOverviewScreen,
    Camera: CameraScreen
  },
  {
    initialRouteName: 'InspirationLibrary'
  }
)

const SignedInNavigator = createBottomTabNavigator(
  { 
    Inspiration: InspirationNavigator, 
    Profile: ProfileScreen 
  },
  {
    initialRouteName: 'Inspiration'
  }
)

const AuthNavigator = createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    SignedIn: SignedInNavigator,
    SignedOut: SignedOutNavigator
  },
  {
    initialRouteName: 'AuthLoading'
  }
)

const AppContainer = createAppContainer(AuthNavigator)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
