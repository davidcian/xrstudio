import React, { Component } from 'react';
import { Dimensions, View, StyleSheet, Alert } from 'react-native';
import { Button } from 'react-native-elements';
import { Camera } from 'expo-camera';
import * as Permissions from 'expo-permissions';
import * as ImagePicker from 'expo-image-picker';
import * as firebase from 'firebase';

const { width: windowWidth, height: windowHeight } = Dimensions.get("window")

export class CameraScreen extends Component {
  state = {
    cameraType: Camera.Constants.Type.back
  }

  camera;

  onCameraRef = (camera) => {
    this.camera = camera;
  }

  savePhotoInspiration = async () => {
    const result1 = await Permissions.askAsync(Permissions.CAMERA);
    console.warn(result1);
    const result2 = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    console.warn(result2);
    ImagePicker.launchCameraAsync().then((cancelled, uri, width, height, type) => {
      this.uploadImage(uri, "user_name")
      .then(() => {
        Alert.alert("Success");
      })
      .catch((error) => {
        Alert.alert(error);
      })
    });
  }

  uploadImage = async (uri, imageName) => {
    const response = await fetch(uri);
    const blob = await response.blob();
    var ref = firebase.storage().ref().child("images/" + imageName);
    return ref.put(blob);
  }

  render() {
    return (
      <View style={styles.cameraView}>
        <Camera style={{ flex: 1 }} type={this.state.cameraType} ref={this.onCameraRef} />

        <View style={styles.cameraOverlay}>
          <Button style={{ alignSelf: 'center' }} title="Snap" onPress={this.savePhotoInspiration} />
        </View>
      </View>
    )
  }
} 

const styles = StyleSheet.create({
  cameraView: {
    flex: 1
  },
  cameraOverlay: {
    flex: 1,
    justifyContent: 'flex-end',
    width: windowWidth,
    height: windowHeight - 100,
    position: 'absolute'
  }
});