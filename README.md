XRStudio consists of a Unity scene serving as the artist's workshop and a
companion iPhone app for sourcing inspiration.

Features include:

- Project thumbnails consisting of live snapshots of your scenes
- Import pictures into your workspace for a virtual moodboard!
- Import text and turn it into ambiental modifiers
- Offer clients virtual tours of your workshop or your creative product
- Source your inspiration directly from the product owner!

Don't forget to sign your workshop with your 3D artist signature!